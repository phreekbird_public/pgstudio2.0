FROM phreekbird/openjdk8:latest
MAINTAINER phreekbird webmaster@phreekbird.net
# update apt list and get wget and unzip, we will need these later, also grab postgresql-client as this is a client, we may need to use the cli sometimes...
RUN DEBIAN_FRONTEND=noninteractive apt-get update -y && DEBIAN_FRONTEND=noninteractive apt-get install wget unzip postgresql-client -y
# use wget and pull the pgstudio package
RUN wget http://downloads.postgresqlstudio.org/2.0/pgstudio_2.0-bin.zip
# unzip it
RUN unzip pgstudio_2.0-bin.zip
# clean house
RUN rm pgstudio_2.0-bin.zip
# expose port 8080
EXPOSE 8080
# set CATALINA_HOME environment variable
ENV CATALINA_HOME /pgstudio_2.0-bin
# foce the workind dir as CATALINA_HOME
WORKDIR $CATALINA_HOME
# start the pgstudio script
CMD ["bin/catalina.sh", "run"]
