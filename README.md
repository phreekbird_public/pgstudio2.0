Docker file for: https://hub.docker.com/r/phreekbird/pgstudio2.0/

Sets up PGStudio in docker: http://www.postgresqlstudio.org/

Requires phreekbird/openjre7

Installs wget and unzip

Sets up pgstudio 2.0.

Your mileage may vary, use at your own risk!

'docker run -dit -p 8080:8080 phreekbird/pgstudio2.0'
